<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<!-- ace styles -->

		<link rel="stylesheet" href="assets/css/ace.min.css" />
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="css/jNotify.jquery.css" />  <!-- 提示框插件 -->
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<script src="assets/js/jquery-2.0.3.min.js"></script>
<title>管理中心 | 用户数据录入</title>
</head>
<body>
		<%@ include file="/include/ace_top_header.jsp" %>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<%@ include file="/include/ace_left_header.jsp" %>

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="#">管理中心</a>
							</li>
							<li class="active">控制台</li>
						</ul><!-- .breadcrumb -->

					
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
								控制台
								<small>
									<i class="icon-double-angle-right"></i>
									 用户数据录入
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 用户账户 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="user account" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 用户密码 </label>

										<div class="col-sm-9">
											<input type="password" id="form-field-2" placeholder="user password" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-3"> 用户昵称 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-3" placeholder="user nickname" class="col-xs-10 col-sm-5" />
										</div>
									</div>
									
									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-4"> 用户电话 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-4" placeholder="user phone" class="col-xs-10 col-sm-5" />
										</div>
									</div>
									
									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 用户邮箱 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-5" placeholder="user email" class="col-xs-10 col-sm-5" />
										</div>
									</div>
									
									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-6"> 用户部门 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-6" placeholder="user department" class="col-xs-10 col-sm-5" />
										</div>
									</div>
									
                                    <div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-1"> 打卡权限 </label>
										<div class="col-xs-3">
													<label>
														<input name="switch-field-1" id="switch-field-1" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>
									
									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-2"> 门禁权限 </label>
										<div class="col-xs-3">
													<label>
														<input name="switch-field-2" id="switch-field-2" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>
									
									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-3"> 是否禁用 </label>
										<div class="col-xs-3">
													<label>
														<input name="switch-field-3" id="switch-field-3" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>

                                    <div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-7"> 指纹采集 </label>
										<div class="col-sm-1">
										<button class="btn btn-xs" type="button" id="finger_photo"><i class="icon-camera"></i>采集</button>
										</div>
										<div class="col-sm-2 thumbnail">
                                              <img src="finger/test.png" id="figer_copy" alt="指纹">
                                          </div>
									</div>

									<div class="space-4"></div>
									
									<div class="form-group">
									    <label class="col-sm-3 control-label no-padding-right"> 头像采集 </label>
										<div class="col-sm-9">
										<embed src="http://${ip}/web/swfs/StrobeMediaPlayback.swf" quality="high" bgcolor="#000000" name="StrobeMediaPlayback" allowfullscreen="true" pluginspage="http://www.adobe.com/go/getflashplayer" flashvars="&src=rtmp://${ip}:1935/flash/11:admin:admin&autoHideControlBar=true&streamType=live&autoPlay=true" type="application/x-shockwave-flash" class="col-sm-5">									
										  <div class="col-sm-5 thumbnail">
                                              <img src="" id="img_copy" alt="头像">
                                          </div>
										</div>
										<input type="text" id="cache_photo_url" class="hidden" />
									</div>
									
									<div class="space-4"></div>
                                   <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right"> </label>
										<div class="col-sm-9">
										<button class="btn btn-xs" type="button" id="photo"><i class="icon-camera"></i>拍照</button>
										<button class="btn btn-xs" type="button" id="up_button">上移</button>
										<button class="btn btn-xs" type="button" id="down_button">下移</button>
										<button class="btn btn-xs" type="button" id="left_button">左移</button>
										<button class="btn btn-xs" type="button" id="right_button">右移</button>
										</div>
                                   </div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="button" id="submit">
												<i class="icon-ok bigger-110"></i>
											   录入
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												重置
											</button>
										</div>
									</div>
								</form>
								
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

				
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->


		<!--[if IE]>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<iframe src="" style="display:none"></iframe>
		
		<script src="js/jNotify.jquery.js"></script>  <!-- 提示框插件 -->
		<script>
		
		$(function(){
			var $input_checkbox = $("input[type='checkbox']");
			
			$input_checkbox.click(function(){
				if($(this).prop("checked")){
					$(this).val(1);
				}else{
					$(this).val(0);
				}
			});
		});
		
		
		
		function Photograph()
		{
			var $img_copy = $("#img_copy");
			var img_photo = new Image();
			img_photo.src = "http://${ip}/tmpfs/auto.jpg?" + (new Date()).getTime();
			$img_copy[0].src = img_photo.src;
			
			$.get("User_photo_cache",function(data,status){
				if(data.result == true){
					alert("数据缓存成功");
				}else{
					//缓存失败
				}
			},"json");
		}
		
		function ptzcmdSubmit(casename)
		{
			var $iframe = $("iframe");
			$iframe[0].src="http://${ip}/web/cgi-bin/hi3510/ptzctrl.cgi?-step=0&-act=" + casename;
		}
		
		$(function(){
			var $photo = $("#photo");
			var $up_button = $("#up_button");
			var $down_button = $("#down_button");
			var $left_button = $("#left_button");
			var $right_button = $("#right_button");
			
			$up_button.mousedown(function(){
				ptzcmdSubmit("up");
				});
			$up_button.mouseup(function(){ptzcmdSubmit("stop");});
			$up_button.mouseleave(function(){ptzcmdSubmit("stop");});
			
			$down_button.mousedown(function(){
				ptzcmdSubmit("down");
				});
			$down_button.mouseup(function(){ptzcmdSubmit("stop");});
			$down_button.mouseleave(function(){ptzcmdSubmit("stop");});
			
			$left_button.mousedown(function(){
				ptzcmdSubmit("left");
				});
			$left_button.mouseup(function(){ptzcmdSubmit("stop");});
			$left_button.mouseleave(function(){ptzcmdSubmit("stop");});
			
			$right_button.mousedown(function(){
				ptzcmdSubmit("right");
				});
			$right_button.mouseup(function(){ptzcmdSubmit("stop");});
			$right_button.mouseleave(function(){ptzcmdSubmit("stop");});
			
			$photo.click(function(){  //拍照
				Photograph();
			});
			
			
		});
		
		
		$(function(){
			$("#finger_photo").click(function(){
				
				//jNotify("操作提醒!",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
				$.ajax({
					url:"Finger_collect",
					type:"get",
					async:false,
					dataType:"json",
					success:function(data,status){
						if(data.result==true){
							//
							jSuccess("指纹采集成功",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
							$("#figer_copy")[0].src = data.user_fingerprint;
							$("#figer_copy")[0].alt = data.user_fingerprint;
							$("#figer_copy").parent()[0].id = data.finger_id;
						}else{
							jError("指纹采集失败",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
						}
					}
				});
				
			});
		});
		
		$(function(){
			var $submit = $("#submit");
			$submit.click(function(){
				var user_account = $("#form-field-1").val();
				var user_password = $("#form-field-2").val();
				var user_nickname = $("#form-field-3").val();
				var user_photo = "img_cache/test.jpeg";
				var user_fingerprint = $("#figer_copy")[0].alt;
				var user_phone = $("#form-field-4").val();
				var user_email = $("#form-field-5").val();
				var user_department = $("#form-field-6").val();
				var clock_jurisdiction = $("#switch-field-1").val();
				var door_jurisdiction = $("#switch-field-2").val();
				var user_disable = $("#switch-field-3").val();
				var finger_id = $("#figer_copy").parent()[0].id;
				
				var data={
						user_account:user_account,
						user_password:user_password,
						user_nickname:user_nickname,
						user_photo:user_photo,
						user_fingerprint:user_fingerprint,
						user_phone:user_phone,
						user_email:user_email,
						user_department:user_department,
						clock_jurisdiction:clock_jurisdiction,
						door_jurisdiction:door_jurisdiction,
						user_disable:user_disable,
						finger_id:finger_id
				}
				$.post("User_add",{data:JSON.stringify(data)},function(data,status){
					if(data.result == true){
						//数据添加成功
						jSuccess("数据添加成功",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
					}else{
						alert("数据添加失败");
					}
				},"json");
				
				
			});
			
		});
		
		
		
		</script>
</body>
</html>