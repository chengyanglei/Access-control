<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<!-- ace styles -->

		<link rel="stylesheet" href="assets/css/ace.min.css" />
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="css/jNotify.jquery.css" />  <!-- 提示框插件 -->
		<script src="assets/js/jquery-2.0.3.min.js"></script>
<title>管理中心 | 管理员编辑</title>
</head>
<body>
		<%@ include file="/include/ace_top_header.jsp" %>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<%@ include file="/include/ace_left_header.jsp" %>

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="#">管理中心</a>
							</li>
							<li class="active">控制台</li>
						</ul><!-- .breadcrumb -->

					
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
								控制台
								<small>
									<i class="icon-double-angle-right"></i>
									 管理员编辑
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
<div class="table-responsive">
											<table id="sample-table-1" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>管理员昵称</th>
														<th>管理员账户</th>
														<th>超级权限</th>
														<th>用户管理权限</th>
														<th>设备管理权限</th>
                                                        <th>基础权限</th>
														<th></th>
													</tr>
												</thead>

												<tbody>
												<c:forEach items="${admin_infs}" var="admin_inf" varStatus="status">
													<tr id="${admin_inf.admin_account}">
														<td><input type="text" value="${admin_inf.admin_nickname}" /></td>
														<td><input type="text" value="${admin_inf.admin_account}" /></td>
														
														<td>
														<label>
														<input class="ace ace-switch ace-switch-4" type="checkbox" <c:if test="${admin_inf.super_jurisdiction == 1}">checked value="1"</c:if> <c:if test="${admin_inf.super_jurisdiction == 0}">value="0"</c:if> />
														<span class="lbl"></span>
													    </label>
													    </td>
														<td>
														<label>
														<input class="ace ace-switch ace-switch-4" type="checkbox" <c:if test="${admin_inf.user_management_jurisdiction == 1}">checked value="1"</c:if> <c:if test="${admin_inf.user_management_jurisdiction == 0}">value="0"</c:if> />
														<span class="lbl"></span>
													    </label>
													    </td>

														<td>
														<label>
														<input class="ace ace-switch ace-switch-4" type="checkbox" <c:if test="${admin_inf.device_management_jurisdiction == 1}">checked value="1"</c:if> <c:if test="${admin_inf.device_management_jurisdiction == 0}">value="0"</c:if> />
														<span class="lbl"></span>
													    </label>
													    </td>
													    
													    <td>
														<label>
														<input class="ace ace-switch ace-switch-4" type="checkbox" <c:if test="${admin_inf.basic_jurisdiction == 1}">checked value="1"</c:if> <c:if test="${admin_inf.basic_jurisdiction == 0}">value="0"</c:if> />
														<span class="lbl"></span>
													    </label>
													    </td>

														<td>
															<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
																<button class="btn btn-xs btn-success">
																	<i class="icon-ok bigger-120"></i>
																</button>

																<button class="btn btn-xs btn-danger">
																	<i class="icon-trash bigger-120"></i>
																</button>
															</div>
														</td>
													</tr>
													</c:forEach>
												</tbody>
											</table>
										</div><!-- /.table-responsive -->
								
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

				
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if IE]>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script src="js/jNotify.jquery.js"></script>  <!-- 提示框插件 -->
		<script>
		$(function(){
			var $input_checkbox = $("input[type='checkbox']");
			
			$input_checkbox.click(function(){
				if($(this).prop("checked")){
					$(this).val(1);
				}else{
					$(this).val(0);
				}
			});
		});
		
		
		$(function(){
			var $edit = $("button[class='btn btn-xs btn-success']");
			
			$edit.click(function(){
				var this_button = $(this);
				var old_account = $(this).parents("tr")[0].id;
				var admin_nickname = $(this).parents("td").siblings().eq(0).find("input").val();
				var admin_account = $(this).parents("td").siblings().eq(1).find("input").val();
				var super_jurisdiction = $(this).parents("td").siblings().eq(2).find("input").val();
				var user_management_jurisdiction = $(this).parents("td").siblings().eq(3).find("input").val();
				var device_management_jurisdiction = $(this).parents("td").siblings().eq(4).find("input").val();
				var basic_jurisdiction = $(this).parents("td").siblings().eq(5).find("input").val();
				
				$.post("Admin_edit",{old_account:old_account ,admin_account:admin_account ,admin_nickname:admin_nickname ,super_jurisdiction:super_jurisdiction ,user_management_jurisdiction:user_management_jurisdiction ,device_management_jurisdiction:device_management_jurisdiction ,basic_jurisdiction:basic_jurisdiction},function(data,status){
					if(data.result == true){
						this_button.parents("tr")[0].id = admin_account;
						jSuccess("信息修改成功",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
					}else{
						//信息修改失败
						jError("信息修改失败",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
					}
				},"json");
				
			});
			
			
		});
		
		
		$(function(){
			var $delete = $("button[class='btn btn-xs btn-danger']");
			
			$delete.click(function(){
				var admin_account = $(this).parents("tr")[0].id;
				
				$.post("Admin_delete",{admin_account:admin_account},function(data,status){
					if(data.result == true){
						//jSuccess("信息删除成功",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
						window.location.reload();
					}else{
						jError("信息删除失败",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
					}
				},"json");
			});
		});
		
		
		</script>
</body>
</html>