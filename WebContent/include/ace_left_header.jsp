<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="sidebar" id="sidebar">
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<li class="active">
							<a href="admin_index.jsp">
								<i class="icon-dashboard"></i>
								<span class="menu-text"> 管理中心  </span>
							</a>
						</li>

						<c:if test="${super_jurisdiction == 1}">
						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-text-width"></i>
								<span class="menu-text"> 权限管理 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="#" class="dropdown-toggle">
										<i class="icon-double-angle-right"></i>

										管理员权限管理
										<b class="arrow icon-angle-down"></b>
									</a>

									<ul class="submenu">
										<li>
											<a href="Admin_jurisdiction_add">
												<i class="icon-leaf"></i>
												管理员添加
											</a>
										</li>

										<li>
											<a href="Admin_jurisdiction_edit">
												<i class="icon-leaf"></i>
												管理员编辑
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						</c:if>


<c:if test="${device_management_jurisdiction == 1}">
						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-desktop"></i>
								<span class="menu-text"> 设备管理 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="Admin_fingerprint">
										<i class="icon-double-angle-right"></i>
										指纹设备
									</a>
								</li>

								<li>
									<a href="admin_video.jsp">
										<i class="icon-double-angle-right"></i>
										视频设备
									</a>
								</li>

							</ul>
						</li>

</c:if>

<c:if test="${user_management_jurisdiction == 1}">
						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-list"></i>
								<span class="menu-text"> 用户数据管理</span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="Data_entry">
										<i class="icon-double-angle-right"></i>
										数据录入
									</a>
								</li>

								<li>
									<a href="Admin_data_edit">
										<i class="icon-double-angle-right"></i>
										用户数据编辑
									</a>
								</li>
							</ul>
						</li>

</c:if>

<c:if test="${basic_jurisdiction == 1}">
						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-tag"></i>
								<span class="menu-text"> 其他信息 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
							<li>
									<a href="System_contorl">
										<i class="icon-double-angle-right"></i>
										系统管理
									</a>
								</li>
								<li>
									<a href="Admin_data_statistics">
										<i class="icon-double-angle-right"></i>
										数据统计
									</a>
								</li>


							</ul>
						</li>
</c:if>

<c:if test="${basic_jurisdiction == 1}">
<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-tag"></i>
								<span class="menu-text"> 账户设置 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="admin_account_safe.jsp">
										<i class="icon-double-angle-right"></i>
										账户安全
									</a>
								</li>

							</ul>
						</li>
						</c:if>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>