<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<!-- ace styles -->

		<link rel="stylesheet" href="assets/css/ace.min.css" />
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="css/jNotify.jquery.css" />  <!-- 提示框插件 -->
		<script src="assets/js/jquery-2.0.3.min.js"></script>
<title>管理中心 | 管理员添加</title>
</head>
<body>
		<%@ include file="/include/ace_top_header.jsp" %>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<%@ include file="/include/ace_left_header.jsp" %>

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="#">管理中心</a>
							</li>
							<li class="active">控制台</li>
						</ul><!-- .breadcrumb -->

					
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
								控制台
								<small>
									<i class="icon-double-angle-right"></i>
									 管理员添加
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 管理员账户 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="admin name" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 管理员密码 </label>

										<div class="col-sm-9">
											<input type="password" id="form-field-2" placeholder="User password" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-3"> 管理员昵称 </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-3" placeholder="User nickname" class="col-xs-10 col-sm-5" />
										</div>
									</div>

                                    <div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-1"> 超级权限 </label>

										<div class="col-xs-3">
													<label>
														<input name="switch-field-1" id="switch-field-1" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-2"> 用户管理权限 </label>

										<div class="col-xs-3">
													<label>
														<input name="switch-field-2" id="switch-field-2" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-3"> 设备管理权限 </label>

										<div class="col-xs-3">
													<label>
														<input name="switch-field-3" id="switch-field-3" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="switch-field-4"> 基础权限 </label>

										<div class="col-xs-3">
													<label>
														<input name="switch-field-4" id="switch-field-4" class="ace ace-switch ace-switch-4" type="checkbox" value="0" />
														<span class="lbl"></span>
													</label>
										</div>
									</div>
									

									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="button" id="submit">
												<i class="icon-ok bigger-110"></i>
											   添加
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												重置
											</button>
										</div>
									</div>
								</form>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

				
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->


		<!--[if IE]>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>
<script src="js/jNotify.jquery.js"></script>  <!-- 提示框插件 -->
		<!-- inline scripts related to this page -->
		<script src="js/jquery.md5.js"></script>
		<script>
		
		$(function(){
			var $input_checkbox = $("input[type='checkbox']");
			
			$input_checkbox.click(function(){
				if($(this).prop("checked")){
					$(this).val(1);
				}else{
					$(this).val(0);
				}
			});
		});
		
		
		
		
		$(function(){
			var $admin_account = $("#form-field-1");
			var $admin_password = $("#form-field-2");
			var $admin_nickname = $("#form-field-3");
			var $super_jurisdiction = $("#switch-field-1");
			var $user_management_jurisdiction = $("#switch-field-2");
			var $device_management_jurisdiction = $("#switch-field-3");
			var $basic_jurisdiction = $("#switch-field-4");
			
			var $submit = $("#submit");
			
			$submit.click(function(){
				var admin_account = $admin_account.val();
				var admin_password = $.md5($admin_password.val());
				var admin_nickname = $admin_nickname.val();
				var super_jurisdiction = $super_jurisdiction.val();
				var user_management_jurisdiction = $user_management_jurisdiction.val();
				var device_management_jurisdiction = $device_management_jurisdiction.val();
				var basic_jurisdiction = $basic_jurisdiction.val();
				
				$.post("Admin_add",{admin_account:admin_account,admin_password:admin_password,admin_nickname:admin_nickname,super_jurisdiction:super_jurisdiction,user_management_jurisdiction:user_management_jurisdiction,device_management_jurisdiction:device_management_jurisdiction,basic_jurisdiction:basic_jurisdiction},function(data,status){
					if(data.result == true){
						jSuccess("添加成功",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
					}else{
						jError("添加失败",{HorizontalPosition : "center",VerticalPosition : "center",ShowOverlay : false});
					}
				},"json");
				
			});
			
		});
		</script>
		
		
</body>
</html>