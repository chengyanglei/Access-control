package bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

import DB.DB_connection_pool;

public class System_logs {
	
	public static final int Access_Jurisdiction = 0;
	public static final int Clock_jurisdiction = 1;
	public static final int User_disable = 2;
	public static final int No_finger = 3;
	public static final int Access_success = 4;
	public static final int Clock_success = 5;
	
	private static List<System_log> system_logs = new ArrayList<System_log>();
	public static List<System_log> getSystem_logs() {
		return system_logs;
	}

	public static void setSystem_logs(List<System_log> system_logs) {
		System_logs.system_logs = system_logs;
	}

	/*
	 * index  0门禁，1打卡
	 * user_id  用户id
	 * finger_tf  指纹是否存在
	 */
	public static void system_log(int index, String user_id, int finger_tf){

		if(finger_tf == 1){
			if(Jurisdiction_select(2,user_id)){
				if(index == 0){
					if(Jurisdiction_select(0,user_id)){
						print_log("门禁成功", user_id, 1);	
					}else{
						print_log("无门禁权限", user_id, 0);
					}
				}else if(index == 1){
					if(Jurisdiction_select(1,user_id)){
						print_log("打卡成功", user_id, 1);
					}else{
						print_log("无打卡权限", user_id, 0);
					}
				}else{
					//输入错误
				}
			}else{
				print_log("用户被禁用", user_id, 0);
			}
		}else{
			print_log("无用户数据", user_id, 0);
		}
		
	}
	
	/*
	 * 权限查询
	 * index 0门禁权限，1打卡权限，2是否禁用
	 */
	public static boolean Jurisdiction_select(int index, String user_id){
		String select_sql = "select * from user_data where user_id = '"+user_id+"'";
		
		try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_select= conn.prepareStatement(select_sql);
			ResultSet rs = ps_select.executeQuery();
			if(rs.next()){
				if(index == 0){
					if(rs.getInt("door_jurisdiction") == 1){
						rs.close();
						ps_select.close();
						conn.close();
						return true;
					}else{
						rs.close();
						ps_select.close();
						conn.close();
						return false;
					}
				}else if(index == 1){
					if(rs.getInt("clock_jurisdiction") == 1){
						rs.close();
						ps_select.close();
						conn.close();
						return true;
					}else{
						rs.close();
						ps_select.close();
						conn.close();
						return false;
					}
				}else if(index == 2){
					if(rs.getInt("user_disable") == 1){
						rs.close();
						ps_select.close();
						conn.close();
						return true;
					}else{
						rs.close();
						ps_select.close();
						conn.close();
						return false;
					}
				}
			}
			rs.close();
			ps_select.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/*
	 * 打印日志
	 * reason 原因
	 * user_id 用户id
	 * pass_tf 是否通过
	 */
	public static void print_log(String reason, String user_id, int pass_tf){
		String insert_sql = "insert into system_log(log_id, date, pass_tf, reason, user_id) values(?,?,?,?,?)";
		
		Clock clock = Clock.systemUTC();
		Timestamp date = new Timestamp(clock.millis());     //当前时间
		String log_id = String.valueOf((int)(Math.random()*900000)+100000)+clock.millis();  //编号
		try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_insert = conn.prepareStatement(insert_sql);
			ps_insert.setString(1, log_id);
			ps_insert.setTimestamp(2, date);
			ps_insert.setInt(3, pass_tf);
			ps_insert.setString(4, reason);
			ps_insert.setString(5, user_id);
			ps_insert.executeUpdate();
			
			System_log system_log = new System_log();
			system_log.setDate(date);
			system_log.setLog_id(log_id);
			system_log.setPass_tf(pass_tf);
			system_log.setReason(reason);
			system_log.setUser_account(user_id);
			
			system_logs.add(system_log);
			
			ps_insert.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
