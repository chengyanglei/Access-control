package bean;

import java.util.ArrayList;
import java.util.List;

public class Fingerprint_templates {

	private static List<Fingerprint_template> fingerprint_templates_cache  = new ArrayList<Fingerprint_template>();
	private static List<Fingerprint_template> fingerprint_templates_DB  = new ArrayList<Fingerprint_template>();
	public static List<Fingerprint_template> getFingerprint_templates_cache() {
		return fingerprint_templates_cache;
	}
	public static void setFingerprint_templates_cache(
			List<Fingerprint_template> fingerprint_templates_cache) {
		Fingerprint_templates.fingerprint_templates_cache = fingerprint_templates_cache;
	}
	public static List<Fingerprint_template> getFingerprint_templates_DB() {
		return fingerprint_templates_DB;
	}
	public static void setFingerprint_templates_DB(
			List<Fingerprint_template> fingerprint_templates_DB) {
		Fingerprint_templates.fingerprint_templates_DB = fingerprint_templates_DB;
	}

}
