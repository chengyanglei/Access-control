package admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DB.DB_connection_pool;
import bean.User_inf;

@WebServlet("/Admin_data_edit")
public class Admin_data_edit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String select_sql = "select * from user_data";
		
		List<User_inf> user_infs = new ArrayList<User_inf>();
		try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_select= conn.prepareStatement(select_sql);
			ResultSet rs = ps_select.executeQuery();
			
			while(rs.next()){
				User_inf user_inf = new User_inf();
				user_inf.setUser_id(rs.getString("user_id"));
				user_inf.setUser_account(rs.getString("user_account"));
				user_inf.setUser_department(rs.getString("user_department"));
				user_inf.setUser_email(rs.getString("user_email"));
				user_inf.setUser_nickname(rs.getString("user_nickname"));
				user_inf.setUser_password(rs.getString("user_password"));
				user_inf.setUser_phone(rs.getString("user_phone"));
				user_inf.setUser_photo(rs.getString("user_photo"));
				user_inf.setUser_register_date(rs.getTimestamp("user_register_date"));
				user_inf.setClock_jurisdiction(rs.getInt("clock_jurisdiction"));
				user_inf.setDoor_jurisdiction(rs.getInt("door_jurisdiction"));
				user_inf.setUser_disable(rs.getInt("user_disable"));
				user_inf.setUser_fingerprint(rs.getString("user_fingerprint"));
				
				user_infs.add(user_inf);
			}
			rs.close();
			ps_select.close();
			conn.close();
			
			request.setAttribute("user_infs", user_infs);
			request.getRequestDispatcher("/admin_data_edit.jsp").forward(request, response);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
