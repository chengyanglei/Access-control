package admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DB.DB_connection_pool;
import bean.System_log;
import bean.System_logs;

@WebServlet("/Admin_data_statistics")
public class Admin_data_statistics extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		for(System_log system_log : System_logs.getSystem_logs()){
			String select_sql = "select * from user_data where user_id = '"+system_log.getUser_account()+"'";
			try {
				Connection conn = DB_connection_pool.data_pool.getConnection();
				PreparedStatement ps_select= conn.prepareStatement(select_sql);
				ResultSet rs = ps_select.executeQuery();
				
				while(rs.next()){
					system_log.setUser_name(rs.getString("user_nickname"));
				}
				
				rs.close();
				ps_select.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		request.setAttribute("system_logs", System_logs.getSystem_logs());
		request.getRequestDispatcher("/admin_data_statistics.jsp").forward(request, response);
		
	}

}
