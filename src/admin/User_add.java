package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Clock;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import bean.Fingerprint_template;
import bean.Fingerprint_templates;
import DB.DB_connection_pool;


@WebServlet("/User_add")
public class User_add extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		//System.out.println(request.getParameter("data"));
		JSONObject data = new JSONObject(request.getParameter("data"));
		
		String insert_sql = "insert into user_data(user_account,user_password,user_nickname,"
				+ "user_photo,user_fingerprint,user_phone,user_email,user_department,"
				+ "user_register_date,clock_jurisdiction,door_jurisdiction,user_disable,fingerprint_template,user_id) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		Clock clock = Clock.systemUTC();
		Timestamp user_register_date = new Timestamp(clock.millis());     //当前时间
		
		try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_insert = conn.prepareStatement(insert_sql);
			ps_insert.setString(1, data.getString("user_account"));
			ps_insert.setString(2, data.getString("user_password"));
			ps_insert.setString(3, data.getString("user_nickname"));
			ps_insert.setString(4, data.getString("user_photo"));
			ps_insert.setString(5, data.getString("user_fingerprint"));
			ps_insert.setString(6, data.getString("user_phone"));
			ps_insert.setString(7, data.getString("user_email"));
			ps_insert.setString(8, data.getString("user_department"));
			ps_insert.setTimestamp(9, user_register_date);
			ps_insert.setInt(10, data.getInt("clock_jurisdiction"));
			ps_insert.setInt(11, data.getInt("door_jurisdiction"));
			ps_insert.setInt(12, data.getInt("user_disable"));
			for(Fingerprint_template f : Fingerprint_templates.getFingerprint_templates_cache()){
				if(f.getFingerprint_template_id().equals(data.getString("finger_id"))){
					Fingerprint_templates.getFingerprint_templates_DB().add(f);
					ps_insert.setBytes(13, f.getFingerprint_template());
				}
			}
			ps_insert.setString(14, data.getString("finger_id"));   //user_id与finger_id同
			
			ps_insert.executeUpdate();
			
			jsonObj.put("result", true);
		} catch (SQLException e) {
			jsonObj.put("result", false);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.print(jsonObj.toString());
	}

}
