package admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Admin_fingerprint")
public class Admin_fingerprint extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getServletContext().getAttribute("sensoropen")!=null){
			request.setAttribute("finger_dv", 0);
		}else{
			request.setAttribute("finger_dv", 1);
		}
		
		request.getRequestDispatcher("admin_fingerprint.jsp").forward(request, response);
	}

}
