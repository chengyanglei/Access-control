package admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.System_contorl;

import org.json.JSONObject;

import access_clock.Access_thread;
import access_clock.Clock_thread;

@WebServlet("/System_contorl_on_off")
public class System_contorl_on_off extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		
		String Access_control_str;
		if((Access_control_str = request.getParameter("Access_control"))!=null){
			int Access_control = Integer.parseInt(Access_control_str);
			if(Access_control == 1){
				//开启门禁线程
				System.out.println("门禁系统开启");
				System_contorl.setAccess_control(1);
				request.getServletContext().setAttribute("Access_control", 1);
				Access_thread.run((int)(request.getServletContext().getAttribute("sensoropen")));
				
			}else if(Access_control == 0){
				//结束门禁线程
				System_contorl.setAccess_control(0);
				request.getServletContext().setAttribute("Access_control", 0);
			}
		}
		String clock_control_str;
		if((clock_control_str = request.getParameter("clock_control"))!=null){
			int clock_control = Integer.parseInt(clock_control_str);
			if(clock_control == 1){
				//开启打卡线程
				System.out.println("打卡系统开启");
				System_contorl.setClock_control(1);
				request.getServletContext().setAttribute("clock_control", 1);
				Clock_thread.run((int)(request.getServletContext().getAttribute("sensoropen")));
				
			}else if(clock_control == 0){
				//结束打开线程
				System_contorl.setClock_control(0);
				request.getServletContext().setAttribute("clock_control", 0);
			}
		}
		//System.out.println(Access_control_str);
		jsonObj.put("result", true);
		out.print(jsonObj.toString());
	}

}
