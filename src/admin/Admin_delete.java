package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import DB.DB_connection_pool;

public class Admin_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();

		if((int)(request.getSession().getAttribute("super_jurisdiction")) == 1){
			
			String admin_account = request.getParameter("admin_account");
			
			String delete_sql = "delete from admin_login where admin_account = '"+admin_account+"'";
			
			try {
				Connection conn = DB_connection_pool.data_pool.getConnection();
				PreparedStatement ps_delete = conn.prepareStatement(delete_sql);
				ps_delete.executeUpdate();
				jsonObj.put("result", true);
				
			} catch (SQLException e) {
				jsonObj.put("result", false);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			jsonObj.put("result", false);
		}
		out.print(jsonObj.toString());
	}

}
