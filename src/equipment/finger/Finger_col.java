package equipment.finger;


import java.io.IOException;

import bean.Fingerprint_template;
import bean.Fingerprint_templates;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import equipment.finger.Finger_tool.CLibrary;
import equipment.finger.Finger_tool.ZKFCLibrary;

public class Finger_col {

	/*
	 * path  采集的指纹图像存储位子
	 */
	public Boolean Finger_collect(String finger_id, String path, int sensoropen){
		
		/*int ret = CLibrary.INSTANCE.sensorInit();
		if(ret != 0){
    		System.out.println("设备初始化失败！");
    		return false;
    	}
		int sensoropen = CLibrary.INSTANCE.sensorOpen(0);
		if(sensoropen == 0){
    		CLibrary.INSTANCE.sensorFree();
    		System.out.println("链接设备失败！");
    		return false;
    	}*/
    	int width = CLibrary.INSTANCE.sensorGetParameter(sensoropen, 1);
    	int height = CLibrary.INSTANCE.sensorGetParameter(sensoropen, 2);
    	int imgbuffer = CLibrary.INSTANCE.sensorGetParameter(sensoropen, 106);
		
    	Pointer pSize = new Memory((long)44);
		pSize.setShort(0, (short) width);
		pSize.setShort(2, (short) height);
		pSize.setShort(40, (short) width);
		pSize.setShort(42, (short) height);
    	int bioinit = ZKFCLibrary.zkf.BIOKEY_INIT(0, pSize, null, null, 0);
    	if(bioinit == 0){
    		System.out.println("算法初始化失败");
    		return false;
    	}
		
    	
    	int Fingerprint_migration = ZKFCLibrary.zkf.BIOKEY_SET_PARAMETER(bioinit, 4 , 180);
    	if(Fingerprint_migration == 0){
    		System.out.println("指纹偏移角设置失败!");
    		return false;
    	}
    	
    	byte[] imageBuffer = new byte[imgbuffer];
    	try {
    		//System.out.println("请将手指按压在采集器上!~~~~~~~");
			int cap = Finger_tool.capture(sensoropen, imageBuffer, imgbuffer);
			System.out.println(cap);
			if(cap>0){
				//System.out.println("采集到图像大小为"+cap);
				byte[] template = new byte[2048];
				int act = ZKFCLibrary.zkf.BIOKEY_EXTRACT(bioinit, imageBuffer, template, 1);
				if(act>0){
				Fingerprint_template fingerprint_template = new Fingerprint_template();
				fingerprint_template.setFingerprint_template_id(finger_id);
				fingerprint_template.setFingerprint_template(template);
				Fingerprint_templates.getFingerprint_templates_cache().add(fingerprint_template);
				
				/*for(Fingerprint_template f : Fingerprint_templates.getFingerprint_templates()){
					System.out.println(f.getFingerprint_template_id());
				}*/
				}
				try {
					Finger_tool.writeBitmap(imageBuffer, width, height, path);
				}catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}else if(cap == 0){
				System.out.println("没有采集到图像!");
				return false;
			}else if(cap == -2){
				System.out.println("sensoropen或imageBuffer为空!");
				return false;
			}else if(cap == -3){
				System.out.println("申请的空间小于采集到的图像大小，空间不足!");
				return false;
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	//System.out.println("GGGGGGG1");
    	//CLibrary.INSTANCE.sensorClose();
    	CLibrary.INSTANCE.sensorFree();
    	ZKFCLibrary.zkf.BIOKEY_CLOSE(bioinit);
    	
		return true;
	}
	
	
	

	
}
