package Tool;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class CharacterFilter implements Filter {
	String encoding = null;
	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// 判断字符编码是否为空
				if(encoding != null){
					// 设置request的编码格式
					request.setCharacterEncoding(encoding);
					// 设置response字符编码
		     		response.setContentType("text/html; charset="+encoding);
				}
				// 传递给下一过滤器
		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		encoding = fConfig.getInitParameter("encoding");
	}

}
