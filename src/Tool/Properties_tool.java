package Tool;




import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;



public class Properties_tool extends Properties{
	private static final long serialVersionUID = 1L; 
    private	Properties props_r = new Properties();        //创建Properties对象用于读取文件

	
	public Properties_tool(String path){
		 try {
			 
			InputStreamReader in = new InputStreamReader(new FileInputStream(path), "UTF-8");
			
			props_r.load(in);
			
		} catch (FileNotFoundException e) {
			System.out.printf("读取properties文件失败！");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			System.out.printf("读取properties文件失败！");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public String getvalue(String key){
		//获得key对应的值
		String value;
		value = props_r.getProperty(key, "null");

		return value;
	}
	
	
}
