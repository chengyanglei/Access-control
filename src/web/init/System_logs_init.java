package web.init;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import DB.DB_connection_pool;
import bean.Fingerprint_template;
import bean.Fingerprint_templates;
import bean.System_log;
import bean.System_logs;

@WebServlet(urlPatterns={"/System_logs_init"}, loadOnStartup=1)
public class System_logs_init extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		String select_sql = "select * from system_log";
		
List<System_log> system_logs  = new ArrayList<System_log>();
	    
	    try {
			Connection conn = DB_connection_pool.data_pool.getConnection();
			PreparedStatement ps_select= conn.prepareStatement(select_sql);
			ResultSet rs = ps_select.executeQuery();
			while(rs.next()){
				System_log system_log = new System_log();
				system_log.setLog_id(rs.getString("log_id"));
				system_log.setDate(rs.getTimestamp("date"));
				system_log.setPass_tf(rs.getInt("pass_tf"));
				system_log.setReason(rs.getString("reason"));
				system_log.setUser_account(rs.getString("user_id"));
				system_logs.add(system_log);
			}
			
			System_logs.setSystem_logs(system_logs);
			
			rs.close();
			ps_select.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
